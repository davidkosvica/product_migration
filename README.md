# Product migration
Script that i've recently built for migrating products from old CMS of eshop to a newer one.
Some classes containing sensitive infomration (database structure) are purposefully missing.

1. Script accesses xml feed files (www/xml_feed/).
2. Using simple xml libary and regular expressions it then parses each product and creates associative array with the price of the product and attributes
3. It then accesses database of the new eshop and inserts new products