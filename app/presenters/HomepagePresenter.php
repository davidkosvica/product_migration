<?php

namespace App\Presenters;
use App\Model\InsertingVariants;
use App\Model\ProductMigration;
use App\Model\PicturesConventor;
use App\Model\InsertingTemplates;
use App\Model\AccessXmlFiles;


class HomepagePresenter extends BasePresenter
{
	private $picturesConvertor;
	private $productMigration;
	private $insertVariants;
	private $accessXmlFiles;
	private $insertTemplates;
	
	public function __construct(PicturesConventor $picturesConvertor, 
			ProductMigration $productMigration, 
			InsertingVariants $insertVariants, 
			InsertingTemplates $insertTemplates, 
			AccessXmlFiles $accessXmlFiles) {
		parent::__construct();
		$this->picturesConvertor = $picturesConvertor;
		$this->productMigration = $productMigration;
		$this->insertVariants = $insertVariants;
		$this->insertTemplates = $insertTemplates;
		$this->accessXmlFiles = $accessXmlFiles;
	}
	
	
	public function getUniqueValues($parsedCategoriesOfItems) {
		foreach($parsedCategoriesOfItems as $categoryName => $parsedItems) {
			$categoriesWithUniqueValsOfAttrs[$categoryName] = [];
			foreach ($parsedItems as $parsedItem) {
				$variantsOfParsedItem = $parsedItem['variants'];
				$uniqueValsOfAttrsCategory = $this->getUniqueValsOfAttrsForSpecificCategory($variantsOfParsedItem, $categoriesWithUniqueValsOfAttrs[$categoryName]);
				$categoriesWithUniqueValsOfAttrs[$categoryName] = $uniqueValsOfAttrsCategory;
			}
		}
		
		return $categoriesWithUniqueValsOfAttrs;
	}
	
	
	private function getUniqueValsOfAttrsForSpecificCategory($variants, $uniqueValsOfAttrsFromSpecificCategory) {
		foreach ($variants as $variant) {
			$uniqueValsOfAttrsFromSpecificCategory = $this->getNewUniqueValsForAttrs($variant, $uniqueValsOfAttrsFromSpecificCategory);
		}
		
		return $uniqueValsOfAttrsFromSpecificCategory;
	}
	
	private function getNewUniqueValsForAttrs($variant, $uniqueValsOfAttrsFromSpecificCategory) {
		foreach ($variant as $attrName => $value) {
			if (!array_key_exists($attrName, $uniqueValsOfAttrsFromSpecificCategory)) {
				$uniqueValsOfAttrsFromSpecificCategory[$attrName][] = $value;
			}
			
			$uniqueAttributeVals = $uniqueValsOfAttrsFromSpecificCategory[$attrName]; 
			if (!in_array($value, $uniqueAttributeVals)) {
				$uniqueAttributeVals[] = $value;
			} 
			
			$uniqueValsOfAttrsFromSpecificCategory[$attrName] = $uniqueAttributeVals;
		}
		
		
		return $uniqueValsOfAttrsFromSpecificCategory;
	}

	public function renderDefault() {
		$path = 'C:\xampp\htdocs\projekt-obrazky\www\xml_feed';
		$this->accessXmlFiles->loadXmlFiles($path);
		$itemCategories = [
			'Brusle', 
			'Hokejka', 
			'Kalhoty', 
			'Lapačka', 
			'Holeně', 
			'Helma', 
			'Dres', 
			'Betony', 
			'Lokty',
			'Mřížka',
			'Ramena',
			'Rukavice',
		];
		
		
		foreach ($itemCategories as $itemCategory) {
			$parsedItemsWithAttr[$itemCategory] = $this->accessXmlFiles->getItemsWithNameFromXml($itemCategory, "xml-feed-seznam2.xml");
		}
		$this->insertVariants->insertVariantsOfItems($parsedItemsWithAttr);
		
	}
}