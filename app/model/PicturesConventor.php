<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;
use Nette\Utils\Image; 
use Nette\Utils\FileSystem;
use Nette;

/**
 * Description of picturesConventor
 *
 * @author david
 */
class PicturesConventor {
	
	/** @var Nette\Database\Context */
    private $database;

	
	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	public function getWorkingDir() {
		return getcwd();
	}
	
	public function getIdsFromDb() {
		$result = $this->database->query("SELECT items.id, items_files.filename FROM items left join items_files on items.items_files_id = items_files.id")->fetchAll();
		bdump($result);
		return $result;
	} 
	
	public function createDirStructure() {
		ini_set('max_execution_time', 10000);
		$sizes = ['240', '640', '80'];
		rsort($sizes);
		FileSystem::createDir('files');
		chdir(getcwd() . '/files');
		FileSystem::createDir('sortiment');
		chdir(getcwd() . '/sortiment');
		$rows = $this->getIdsFromDb();
		foreach ($rows as $row) {
			FileSystem::createDir($row->id);
			$source = getcwd() . '\\..\\' . $row->filename;
			$destination = getcwd() . '\\' . $row->id . '\\image\\' . $row->filename;
//			bdump($source);
//			bdump($destination);
			FileSystem::copy($source, $destination);
			bdump(getcwd());
			chdir(getcwd() . '\\' . $row->id . '\\' . 'image\\');
			$Image = Image::fromFile($row->filename);
			foreach ($sizes as $size) {
				FileSystem::createDir($size);
                $Image->resize($size, null, Image::SHRINK_ONLY | Image::FIT);
                $Image->sharpen();
				chdir(getcwd() . '\\' . $size . '\\');
                $Image->save($row->filename, 90);
				chdir('..');
			}
			chdir('../..');
		}
		
		
//		bdump(getcwd());

	}
}
