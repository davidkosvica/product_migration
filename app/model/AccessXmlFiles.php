<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Model;
use Nette\Utils\Strings;

/**
 * Description of AccessXMLFiles
 *
 * @author david
 */
class AccessXmlFiles {
	
	private $xmlFiles;
	
	public function __construct() {
	}
	
	public function loadXmlFiles(string $filePath) {
		$this->xmlFiles = $this->getXmlFiles($filePath);
	}
	
	private function getXmlFiles($filePath) {
		$currentDir = getcwd();
		chdir($filePath);
		
		// ziskam nazvy souboru v adresari
		$xmlFiles = scandir(getcwd());
		
		// odstranim ../ a ./ 
		unset($xmlFiles[0]);
		unset($xmlFiles[1]);
		
		$xmlFiles = array_values($xmlFiles);
		$xmlObjects = [];
		foreach ($xmlFiles as $xmlFile) {
			$xmlObjects[$xmlFile] = simplexml_load_file($xmlFile);
		}
		
		chdir($currentDir);
		
		return $xmlObjects;
	}
	
	function openFile(string $filename) {
		$xml = $this->xmlFiles[$filename];
		return $xml;
	}
	
	function parseBrankarskouHokejku($string) {
		$regexPatternBrankarskaHokejka = "~(levá navy 27|pravá navy 27|levá red 27|pravá red 27|Levá 27)~ui";
		if (!preg_match($regexPatternBrankarskaHokejka, $string)) {
			return false;
		}
		
		$orientace = Strings::match($string, '~(Levá|Pravá)~ui');
		$barva = Strings::match($string, '~(navy|red)~ui');
		$delkaZerdi = Strings::match($string, '~(27)~ui');

		$variant = ['Strana' => $orientace[1], 'Délka žerdi' => $delkaZerdi[1]];
		
		if (!is_null($barva)) {
			$variant['Barva'] = $barva[1];
		}

		return $variant;
	}
	
	function parseObycejnouHokejku($string) {
		$stranaRegExp = '~(Levá|Pravá)~ui';
		$zahnutiRegExp = '~(p[0-9][0-9] Ovechkin|p[0-9][0-9] Backstrom|p*[0-9][0-9]a*)~ui';
		$flexRegExp = '~(flex [0-9][0-9][0-9]*)~ui';
		$negripVerzeRegExp = '~(negripová verze)~ui';

		$strana = Strings::match($string, $stranaRegExp)[1];
		$zahnuti = Strings::match($string, $zahnutiRegExp)[1];
		$flex = Strings::match($string, $flexRegExp)[1];
		$verze = Strings::match($string, $negripVerzeRegExp);
		$variant = ['Strana' => $strana, 'Zahnutí' => $zahnuti, 'Flex' => $flex];
		if (!is_null($verze)) {
			$variant['Verze'] = $verze[1];
		}

		return $variant;
	}
	
	function parseHokejky($string) {
		$variant = $this->parseBrankarskouHokejku($string);
		if(!$variant) {
			$variant = $this->parseObycejnouHokejku($string);
		}
		
		return $variant;
	}
	
	function parseBrusle($string) {
		// preformatuji desetinou carku na desetinou tecku (napr.: 5,5 na 5.5)
		$noCommasVariant = str_replace(',', '.', $string);
		// ziskam velikost ('5.5' EE)
		$velikost = Strings::match($noCommasVariant, '~([0-9]+(?:\.[0-9]+)?)~');
		// ziskam sirku (5.5 'EE')
		$sirka = Strings::match($string, '~(EE|D)~');
		$variant = ['Velikost' => $velikost[1]];
		if (!is_null($sirka)) {
			$variant['Šířka'] = $sirka[1];
		}
		return $variant;
	}
	
	function parseKalhoty($string) {
		$barvaRegExp = '~(černé|červené|navy|tmavě modré|modré|blue)~ui';
		$velikostRegExp = '~(lt|xl|s|l|m)~ui';
		$barva = Strings::match($string, $barvaRegExp)[1];
		$velikost = Strings::match($string, $velikostRegExp)[1];
		
		$variant = ['Velikost' => $velikost, 'Barva' => $barva];

		return $variant;
	}
	
	function parseLapacky($string) {
		$regexPatternBarva = "~(chicago|black/silver|bílá|boston|white)~iu";
		$barva = Strings::match($string, $regexPatternBarva);
		$regexPatternGard = "~(klasický gard|opačný gard)~iu";
		$gard = Strings::match($string, $regexPatternGard);
		$regexPatternRuka = '~(ruka pravá|ruka levá|levá ruka|pravá ruka)~iu';
		$ruka = Strings::match($string, $regexPatternRuka);
		$variant = ['Barva' => $barva[1], 'Gard' => $gard[1], 'Ruka' => $ruka[1]];
		return $variant;
	}
	
	function parseHolene($string) {
		$regexPatternVelikost = '~([0-9]+")~';
		$velikost = Strings::match($string, $regexPatternVelikost);
		$variant = ['Velikost' => $velikost[1]];
		return $variant;
	}
	
	function parseHelma($string) {
		$regexPatternVelikost = "~(S|M|L)~ui";
		$velikost = Strings::match($string, $regexPatternVelikost);
		
		$regexPatternBarva = "~(bílá|modrá \(blue\)|modrá\(royal\)|modrá|černá|červená|navy)~ui";
		$barva = Strings::match($string, $regexPatternBarva);
		
		$variant = ['Velikost' => $velikost[1], 'Barva' => $barva[1]];
		return $variant;
	}
	
	function parseDres($string) {
		$regexPatternVelikost = "~(M|L)~ui";
		$velikost = Strings::match($string, $regexPatternVelikost);
		$variant = ['Velikost' => $velikost[1]];
		
		return $variant;
	}
	
	function parseBetony($string) {
		$regexPatternBarva = "~(CHICAGO|TORONTO|BOSTON|WHITE|TAMPA)~ui";
		$regexPatternVelikost = "~(34\+2|34\+3|35\+2|34\+1|32\+1)~ui";
		$velikost = Strings::match($string, $regexPatternVelikost);
		$barva = Strings::match($string, $regexPatternBarva);
		$variant = ['Velikost' => $velikost[1], 'Barva' => $barva[1]];

		return $variant;
	}
	
	function parseLoktyMrizkyRamena($string) {
		$regexPatternVelikost = "~(xl|s|m|l)~ui";
		$velikost = Strings::match($string, $regexPatternVelikost);
		$variant = ['Velikost' => $velikost[1]];

		return $variant;
	}
	
	function parseRukavice($string) {
		$regexPatternVelikost = '~(10"|11"|12"|13"|14"|15")~ui';
		$velikost = Strings::match($string, $regexPatternVelikost);
		$regexPatternBarva = '~(černé|červené|červená|modré|tmavě modré|tmavé modré|černá\/bílá|navy|černá\/červená|tmavě modrá|modré (blue)|černá\/červená\/bílá|tmavě modrá\/bílá)~ui';
		$barva = Strings::match($string, $regexPatternBarva);
		$variant = ['Velikost' => $velikost[1], 'Barva' => $barva[1]];

		return $variant;
	}
	
	function getVariantsOfItem($xmlNode, $typeOfItem) {
		$variants = [];
		foreach($xmlNode->VARIANT as $variant) {
			$atributyVarianty = (string)$variant->PRODUCTNAMEEXT;
			if ($typeOfItem == 'Brusle') {
				$variant = $this->parseBrusle($atributyVarianty);
			} else if ($typeOfItem == 'Hokejka') {
				$variant = $this->parseHokejky($atributyVarianty);
			} else if ($typeOfItem == 'Kalhoty') {
				$variant = $this->parseKalhoty($atributyVarianty);
			} else if ($typeOfItem == 'Lapačka') {
				$variant = $this->parseLapacky($atributyVarianty);
			} else if ($typeOfItem == 'Holeně') {
				$variant = $this->parseHolene($atributyVarianty);
			} else if ($typeOfItem == 'Helma') {
				$variant = $this->parseHelma($atributyVarianty);
			} else if ($typeOfItem == 'Dres') {
				$variant = $this->parseDres($atributyVarianty);
			} else if ($typeOfItem == 'Betony') {
				$variant = $this->parseBetony($atributyVarianty);
			} else if ($typeOfItem == 'Lokty' || $typeOfItem == 'Mřížka' || $typeOfItem == 'Ramena') {
				$variant = $this->parseLoktyMrizkyRamena($atributyVarianty);
			} else if ($typeOfItem == 'Rukavice') {
				$variant = $this->parseRukavice($atributyVarianty);
			}
//			if (in_array(null, $variant, true)) {
//				bdump($typeOfItem);
//				bdump($atributyVarianty);
//			}
			
			$variants[] = $this->normalizeAttrsOfVariant($variant);
		}
		return $variants;
	}
	
	function normalizeAttrsOfVariant($variant) {
		
		
		foreach ($variant as $attr => $val) {
			
			$variant[$attr] = mb_strtolower($val);
			$valInLowerCase = $variant[$attr];
			$variant[$attr] = $this->getProperValueForImproperOne($valInLowerCase);
			
		}
		
		return $variant;
	}
	
	function getProperValueForImproperOne($val) {
		if ($val == 'white') {
			$properVal = 'bílá';
		} else if ($val == 'tmavé modré' || $val == 'tmavě modré') {
			$properVal = 'tmavě modrá';
		} else if ($val == 'blue' || $val == 'modré') {
			$properVal = 'modrá';
		} else if ($val == 'černé') {
			$properVal = 'černá';
		} else if ($val == 'červené' || $val == 'red') {
			$properVal = 'červená';	
		} else {
			$properVal = $val;
		}
		
		return $properVal;
	}
	
	function getItemsWithNameFromXml(string $typeOfItem, string $fileName) {
		$xml = $this->openFile($fileName);
		$regExPattern = '~' . $typeOfItem . '~iu';
		$items = [];
		foreach($xml->SHOPITEM as $item) {
			if (preg_match($regExPattern, $item->PRODUCTNAME)) {
				$nameOfItem = (string)$item->PRODUCTNAME;
				$price = (string)$item->PRICE;
				$variants = $this->getVariantsOfItem($item, $typeOfItem);
				$items[$nameOfItem] = ['price' => $price, 'variants' => $variants];
			}
		}
		
		return $items;
	}
}
